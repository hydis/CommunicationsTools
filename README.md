# Communications Tools

No personal information needs to be provided when creating an account


 ## Communication Platforms  
  ### Does not require a registration  
   ##### Does not require a registration (Soft)  
 
https://tox.chat/  
https://retroshare.cc/  
https://jami.net/  #fediverse: https://mstdn.io/@Jami  
https://ricochet.im/  
https://briarproject.org/  #fediverse: https://fosstodon.org/@briar  

   ##### Does not require a registration (in browser)  
  
https://meet.jit.si/  
https://jitsi.debian.social/  
https://www.chatcrypt.com/  

  ### Does require a registration  
 
https://dismail.de/  #fediverse: https://social.tchncs.de/@dismail  
https://disroot.org/  #fediverse: https://social.weho.st/@disroot  
https://tutanota.com/  #fediverse: https://mastodon.social/@Tutanota  
https://protonmail.com/  #fediverse: https://mastodon.social/@protonmail  
https://ctemplar.com/  #fediverse: https://mastodon.social/@ctemplar  
https://delta.chat/  #fediverse: https://chaos.social/@delta  
https://element.io/  #fediverse: https://mastodon.matrix.org/@matrix  
https://xmpp.org/ #fediverse: https://fosstodon.org/@xmpp  

